#/bin/env python3
# hashkut 
## script to encrypt .pcap handshakes with hashcat
### Dependecies: hashcat and hashcat-utils
### You need to put the .pcap handshakes in folder "handshakes" in the same directory of
### haskut and the wordlists aka dictionaries in the folder "dictionaries" 

import os

dictionaries = os.listdir("dictionaries")
dicitonary = [ d for d in dictionaries ]
files_in_folder = os.listdir("handshakes")
pcap_handshakes = [ f for f in files_in_folder if "pcap" in f ]

## Defining functions for convert and bruteforce handshakes
def hs_converter():
    """ Uses cap2hccapx to conver pcap
        files into hccapx and than it returns
        a list of files. """
    for h in pcap_handshakes:
        os.system("cap2hccapx handshakes/" +h+ " handshakes/" +h.replace(".pcap",".hccapx_handshakes"))
        print("\nConverting " + h + ".")
        print("File " + h + " converted.")
        hccapx_handshakes = [ f for f in files_in_folder if "hccapx" in f ]
        return hccapx_handshakes

def hashkutting(selected_dictionary):
    """ Brute force using hashcat.
        A dictionary is needed.
        hashkutting(dictionary)"""
    for h in converted_handshakes:
        os.system("hashcat -a 0 -m 2500 handshakes/" +h+ " dictionaries/" +selected_dictionary+ " -O")
        print("Hashkutting handshake :'" + h + "'...")
        print("Done with " +h+ ".")


## Starting program
print("Welcome to hashkut.")
converted_handshakes = hs_converter()
print("\nAvailable converted handshakes:")
print(converted_handshakes)
print("\nAvailable dictionaries:")
print(dictionaries)
selected_dictionary = input('\nWhich dictionary would you like to use? ex: "kutkerst.txt" :')
print("\nWill use the following dictionary: " +selected_dictionary)
hashkutting(selected_dictionary)


